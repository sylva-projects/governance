## Technical Steering Committee

TSC Members roles and election are defined in the Charter here : https://gitlab.com/sylva-projects/sylva/-/blob/main/Project_Sylva_Technical_Charter.pdf

| company | Members | Alternates |
|---------|---------|------------|
| Orange | Guillaume Nevicato  | Mathieu Rohon |
| DT | Vuk Gojnic | Kai Steuernagel |
| Vodafone | Nikoleta Patroni | Amy Lu |
| TIM | Carlo Cavazzoni | Alessandro Capello |
| TEF | Luis Velarde | Javier Ramon Salguero |
| Nokia | Hamalainen, Jouni M | Thoralf Czichy |
| Ericsson | Norbert Niebert | Rihab Banday |

## TSC CoChairs
| Company | Name | Term |
|---------|------|------|
| Orange | Guillaume Nevicato | Dec 2023 - Dec 2024 |
| TIM | Carlo Cavazzoni | Dec 2023 - Dec 2024 |

### 2025 Election Timeline
| Date | Action |
|------|--------|
| Wednesday, Oct. 23 | Start call for nominations; notice given to TSC Members |
| Friday, Nov. 8 | End nomination period, 11:59 pm PDT |
| Monday, Nov. 11 | Slate of respective nominees and ballot or link to an electronic voting system to TSC Members |
| Monday, Nov. 18 | The voting period ends at 11:59 pm PDT |
| Tuesday, Nov. 19 | The election winners announced |
| January 1, 2025 | Newly elected representatives begin their term |

[SUBMIT YOUR NOMINATIONS](https://forms.gle/4Xtyijij98mobxNBA)

_If you can't submit your nominations via G-forms, please email [Naomi](mailto:nwashington@linuxfoundation.org)._

## Committers

| Workgroup | company | Name |
|---------|---------|------------|
| WG01 | Orange  | Mathieu Rohon |
| WG01 | Orange  | François Eleouet |
| WG01 | Orange  | Thomas Morin |
| WG01 | Orange  | Mihai Zaharia |
| WG01 | Orange  | Cristian Manda |
| WG01 | Orange  | Rémi Le Trocquer |
| WG01 | TIM  | Federico Cicchiello |
| WG01 | TIM  | Alessandro Capello|
| WG02 | TEF  | Luis Velarde |
| WG03 | Orange  | Mihai Tuciu |
| WG04 | Red Hat  | Huamin Chen |

WG01 : Cloud Stack Development
WG02 : Validation Center
WG03 : Security Requirements
WG04 : Sustainability

## Contributors
Coming soon
